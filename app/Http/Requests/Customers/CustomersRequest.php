<?php

namespace App\Http\Requests\Customers;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\CustomeFailedValidation;
use Illuminate\Http\Exceptions\HttpResponseException;

class CustomersRequest extends FormRequest
{
    use CustomeFailedValidation;
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules =[
            'first_name' => 'required|max:50',
            'last_name' => 'required|max:50',
            'email' => 'email|required',
            'mobile' => 'digits:10|starts_with:04',
        ];

        // for creation request do this
        if ($this->method() === 'POST') {
            $rules['email'] .= '|unique:customers';
        }


        return  $rules ;
    }

}

