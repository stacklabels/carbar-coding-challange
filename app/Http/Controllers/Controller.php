<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * global function for sending Success Response
     *
     * @param $data
     * @param string $message
     * @return \Illuminate\Http\Response
     */
    public function successResponse($data = [], string $message = ''): \Illuminate\Http\Response
    {
        return response(['data' => $data, 'message' => $message, 'status' => true], 200);
    }

    /**
     * global function for sending Error Response
     *
     * @param $data
     * @param string $message
     * @return \Illuminate\Http\Response
     */
    public function errorResponse(string $message = ''): \Illuminate\Http\Response
    {
        return response(['data' => null, 'message' => $message, 'status' => false], 200);
    }

    /**
     * helper function to Get Exception Message
     *
     * @param \Throwable $exception
     * @return string
     */
    public function getExceptionMessage(\Throwable $exception = null)
    {
        return $exception->getMessage(). ' at ' . $exception->getFile() . ' : ' . $exception->getLine();
    }
}
