<?php

namespace App\Http\Controllers\API;

use App\Models\Customer;
use App\Http\Controllers\Controller;
use App\Http\Resources\CustomersResource;
use App\Http\Requests\Customers\CustomersRequest;
use App\Repositories\Contracts\CustomersRepository;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $customers = Customer::all();
            return $this->successResponse(CustomersResource::collection($customers), 'Retrieved successfully');
        } catch (\Throwable $th) {
            return $this->errorResponse($this->getExceptionMessage($th));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\Customers\CustomersRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomersRequest $request, CustomersRepository $repo)
    {
        try {
            $customer = $repo->store($request->only(CustomersRepository::ACCEPTED_FIELDS));
            return $this->successResponse(new CustomersResource($customer), 'Created successfully');
        } catch (\Throwable $th) {
            return $this->errorResponse($this->getExceptionMessage($th));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        try {
            return $this->successResponse(new CustomersResource($customer), 'Retrieved successfully');
        } catch (\Throwable $th) {
            return $this->errorResponse($this->getExceptionMessage($th));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\Customers\CustomersRequest  $request
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(CustomersRequest $request, Customer $customer, CustomersRepository  $repo)
    {
        try {
            $repo->update($customer, $request->only(CustomersRepository::ACCEPTED_FIELDS));
            return $this->successResponse(new CustomersResource($customer), 'Updated successfully');
        } catch (\Throwable $th) {
            return $this->errorResponse($this->getExceptionMessage($th));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer, CustomersRepository $repo)
    {
        try {
            $repo->delete($customer);
            return $this->successResponse(new CustomersResource($customer), 'Deleted successfully');
        } catch (\Throwable $th) {
            return $this->errorResponse($this->getExceptionMessage($th));
        }
    }
}
