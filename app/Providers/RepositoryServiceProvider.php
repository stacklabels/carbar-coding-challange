<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\CustomersSqlRepository;
use App\Repositories\Contracts\CustomersRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            CustomersRepository::class,
            CustomersSqlRepository::class
        );
        $this->app->bind(CustomersRepository::class, CustomersSqlRepository::class);

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
