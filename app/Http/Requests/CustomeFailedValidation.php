<?php

namespace App\Http\Requests;

use Illuminate\Http\Exceptions\HttpResponseException;

/**
 * Custom Failed Validation for API requests
 */
trait CustomeFailedValidation
{
    /**
     * Handle a failed validation attempt.
     *
     * @param \Illuminate\Contracts\Validation\Validator $validator
     * @return void
     */
    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator) {
        throw new HttpResponseException(response()->json(['status' => false, 'errors' => $validator->errors()], 422));
    }
}

