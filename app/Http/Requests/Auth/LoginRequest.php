<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\CustomeFailedValidation;
use Illuminate\Http\Exceptions\HttpResponseException;


class LoginRequest extends FormRequest
{

    use CustomeFailedValidation;
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'email|required',
            'password' => 'required'
        ];
    }
}
