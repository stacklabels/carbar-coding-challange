<?php

namespace App\Repositories\Contracts;

use App\Models\Customer;

interface CustomersRepository{

    /**
     * this is the list of fields that will be accpted from the user input.
     */
    const ACCEPTED_FIELDS = [
        'first_name',
        'last_name',
        'email',
        'address',
        'mobile'
    ];

    /**
     * interacts with model and stores a Customer.
     *
     * @param array $data
     * @return Customer
     */
    public function store(array $data): Customer;

    /**
     * interacts with model and updates a Customer.
     *
     * @param Customer $customer
     * @param array $data
     * @return Customer
     */
    public function update(Customer $customer, array $data): Customer;

    /**
     * interacts with model and deletes a Customer.
     *
     * @param Customer $customer
     * @return Customer
     */
    public function delete(Customer $customer): Customer;
}
