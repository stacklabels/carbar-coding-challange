<?php

namespace App\Models;

use Laravel\Cashier\Billable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Customer extends Model
{
    use HasFactory;
    use SoftDeletes;
    use Billable;

    protected $hidden = ['deleted_at'];
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'address',
        'mobile',
        'stripe_id',
    ];
}
