
## Overview

this is customers API thats is connected with stripe

The way this task si aachieved is by registering first using register endpiont

then get the access token to interact with the customers functionality

### kick start

* set the stripe credentials in .env  file
* run `composer install`
* run `php artisan migrate`

bang!! ready to go 



## Packages Used

Following are the packages used in to achieve the task

* "laravel/cashier"
* "laravel/passport"

## Api Endpoints

| Task                | Method   | Endpoint                |
| ------------------- | -------- | ----------------------- |
| Create a customer   | `POST` | `/api/customers`      |
| Retrieve a customer | `GET`  | `/api/customers/{ID}` |
| List all customer   | `GET`  | `/api/customers`      |
| Update a customer   | `PUT`  | `/api/customers/{ID}` |
| Delete a customer   | `DELETE`  | `/api/customers/{ID}` |
| Login               | `POST` | `/api/login`          |
| Register            | `POST` | `/api/register`       |
| User            | `POST` | `/api/whoami`       |


