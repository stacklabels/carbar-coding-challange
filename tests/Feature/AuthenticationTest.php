<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Testing\Fluent\AssertableJson;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthenticationTest extends TestCase
{
    /**
     * A basic to see It Return Access Token Incase Of Successfull Registration.
     *
     * @test
     * @return void
     */
    public function it_return_access_token_incase_of_successfull_registration()
    {
        $response = $this->call('POST', route('auth.register'), [
            'name' => 'Sally',
            'email' => 'Sally@email.com',
            'password' => 'password'
        ]);
        $response
            ->assertStatus(200)
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('user')
                    ->has('access_token')
            );
    }

    /**
     * A basic to see It Return Error Incase Of Invalid Email.
     *
     * @test
     * @return void
     */
    public function it_return_error_incase_of_invalid_email()
    {
        $response = $this->call('POST', route('auth.register'), [
            'name' => 'Sally',
            'email' => 'Sallyemail',
            'password' => 'password'
        ]);

        $response
            ->assertStatus(422)
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('status')
                    ->has('errors')
            );
    }

    /**
     * A basic to see It Return Error Incase Of Exisiting Email Registration.
     *
     * @test
     * @return void
     */
    public function it_return_error_incase_of_exisiting_email_registration()
    {
        \App\Models\User::factory(1)->admin()->create();

        $response = $this->call('POST', route('auth.register'), [
            'name' => 'carbar',
            'email' => 'admin@carbar.com.au',
            'password' => 'password'
        ]);
        $response
            ->assertStatus(422)
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('status')
                    ->has('errors')
                    ->has('errors.email')
            )->assertJson([
                "errors" =>  [
                    "email" => [
                        0 => "The email has already been taken."
                    ]
                ]
            ]);
    }


    /**
     * A basic to see It Return Access Token Incase Of Successfull Registration.
     *
     * @test
     * @return void
     */
    public function it_return_access_token_incase_of_successfull_login()
    {
        \App\Models\User::factory(1)->admin()->create();

        $response = $this->call('POST', route('auth.login'), [
            'email' => 'admin@carbar.com.au',
            'password' => 'password'
        ]);
        $response
            ->assertStatus(200)
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('user')
                    ->has('access_token')
            );
    }


    /**
     * A basic to see It Return Error Incase Of Unsuccessfull Login.
     *
     * @test
     * @return void
     */
    public function it_return_error_incase_of_unsuccessfull_login()
    {
        \App\Models\User::factory(1)->admin()->create();

        $response = $this->call('POST', route('auth.login'), [
            'email' => 'admin@carbar.com.au',
            'password' => 'password123'
        ]);
        $response
            ->assertStatus(200)
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
            )->assertJson([ "message" => "Invalid Credentials"]);
    }


}
