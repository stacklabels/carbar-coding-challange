<?php
namespace App\Repositories;

use App\Models\Customer;
use App\Repositories\Contracts\CustomersRepository;


class CustomersSqlRepository  implements CustomersRepository
{

    /**
     * @inheritDoc
     */
    public function store(array $data): Customer
    {
        $customer = Customer::create( $data);
        $customer->createAsStripeCustomer();
        return $customer;
    }

    /**
     * @inheritDoc
     */
    public function update(Customer $customer, array $data): Customer
    {
        $customer->update($data);
        if (isset($data['email'])) {
            $customer->updateStripeCustomer([
                'email' => $data['email']
            ]);
        }
        return $customer;
    }

    /**
     * @inheritDoc
     */
    public function delete(Customer $customer): Customer
    {
        $customer
        ->stripe()
        ->customers
        ->delete($customer->stripe_id);

          $customer->delete();
          return $customer;
    }
}
