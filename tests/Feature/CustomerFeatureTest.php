<?php

namespace Tests\Feature;

use Database\Seeders\CustomerSeeder;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CustomerFeatureTest extends TestCase
{
    protected $token = null;
    /**
     * setUp
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $user = \App\Models\User::factory(1)->admin()->create()->first();
        $this->actingAs($user, 'api');
    }

    /**
     *
     * @test
     * @return void
     */
    public function it_returns_list_of_customer_sucessfully()
    {
        $this->seed(CustomerSeeder::class);
        $response = $this->call('GET', route('customers.index'));
        $response
            ->assertStatus(200)
            ->assertJsonCount(10, 'data')
            ->assertJson([
                "message" => "Retrieved successfully",
                "status" => true
            ]);
    }

    /**
     *
     * @test
     * @return void
     */
    public function it_returns_single_customer_sucessfully()
    {
        $this->seed(CustomerSeeder::class);
        $response = $this->call('GET', route('customers.show', ['customer' => 1]));
        $response
            ->assertStatus(200)
            ->assertJsonFragment(['id' => 1])
            ->assertJson([
                "message" => "Retrieved successfully",
                "status" => true
            ]);
    }


    /**
     *
     * @test
     * @return void
     */
    public function it_returns_error_404_customer_id_is_provided_when_getting()
    {
        $response = $this->call('GET', route('customers.show', ['customer' => 9999]));
        $response->assertStatus(404);
    }


    /**
     *
     * @test
     * @return void
     */
    public function it_returns_error_in_case_of_partial_data()
    {
        $response = $this->call('POST', route('customers.store'), [
            'email' => 'admin@carbar.com.au',
        ]);
        $response
            ->assertStatus(422)
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('errors')->has('status')
            )->assertJson([
                "status" => false,
                "errors" => [
                    "first_name" => [
                        "The first name field is required."
                    ],
                    "last_name" => [
                        "The last name field is required."
                    ]
                ]
            ]);
    }

    /**
     *
     * @test
     * @return void
     */
    public function it_returns_error_in_case_of_invalid_data()
    {
        $response = $this->call('POST', route('customers.store'), [
            'first_name' => 'admin',
            'last_name' => 'carbar',
            'email' => 'adminss',
        ]);
        $response
            ->assertStatus(422)
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('status')
                    ->has('errors')
                    ->has('errors.email')
            );
    }

    /**
     *
     * @test
     * @return void
     */
    public function it_creates_customer_successfully()
    {
        $user = [
            'first_name' => 'admin',
            'last_name' => 'carbar',
            'email' => 'admin@email.com',
        ];
        $response = $this->call('POST', route('customers.store'),$user );
        $response
            ->assertStatus(200)
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('status')
                    ->has('data')
                    ->has('message')
            )->assertJson([ "data" => $user ]);
    }

    /**
     *
     * @test
     * @return void
     */
    public function it_updates_customer_successfully()
    {
        $update = [
            'first_name' => 'john',
            'last_name' => 'smith',
            'email' => 'admin@gmail.com',
        ];
        $this->it_creates_customer_successfully();
        $customer  = \App\Models\Customer::first();
        $response = $this->call('PUT', route('customers.update', ['customer' => $customer->id]), $update );
        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => $update
            ])
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('status')
                    ->has('data')
                    ->has('message')
            );
    }

    /**
     *
     * @test
     * @return void
     */
    public function it_returns_error_404_customer_id_is_provided_when_updating()
    {
        $response = $this->call('PUT', route('customers.show', ['customer' => 9999]));
        $response->assertStatus(404);
    }

    /**
     *
     * @test
     * @return void
     */
    public function it_returns_error_404_customer_id_is_provided_when_deleting()
    {
        $response = $this->call('DELETE', route('customers.show', ['customer' => 9999]));
        $response->assertStatus(404);
    }

    /**
     *
     * @test
     * @return void
     */
    public function it_deletes_customer_successfully()
    {
        $this->it_creates_customer_successfully();
        $customer  = \App\Models\Customer::first();
        $response = $this->call('DELETE', route('customers.destroy', ['customer' => $customer->id]));
        $response
            ->assertStatus(200)
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('status')
                    ->has('data')
                    ->has('message')
            );
    }


}
